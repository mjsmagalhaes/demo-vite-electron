import { mergeConfig } from "vite";
import pkg from "./package.json";

import electronConfigBuilder from "../engine/vite.electron";
import appConfigBuilder from "../engine/vite.app";

export default mergeConfig(
  appConfigBuilder(__dirname, pkg),
  electronConfigBuilder(__dirname)
);
