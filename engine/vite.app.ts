import lo from "lodash";

import { defineConfig } from "vite";
import html from "vite-plugin-virtual-html";

export default (rootdir: string, pkg: any) => {
  return defineConfig({
    plugins: [
      html({
        pages: {
          index: {
            template: "/node_modules/engine/index.html",
            data: {
              title: `Demo`,
              is_dev: true,
            },
          },
        },
        render: (template: string, data: any) => lo.template(template)(data),
      }),
    ],
  });
};
