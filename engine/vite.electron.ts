import path from "path";
import { defineConfig } from "vite";
import electron from "vite-plugin-electron";

export default (rootdir: string) => {
  let storytellerSourceDir = path.resolve(__dirname, "electron");

  console.log("Storyteller Source:", storytellerSourceDir);

  return defineConfig({
    plugins: [
      // renderer(),
      electron({
        main: {
          entry: path.join(storytellerSourceDir, "main.ts"),
        },
        preload: {
          input: path.join(storytellerSourceDir, "preload.ts"),
        },
      }),
    ],
  });
};
